# Homepage Content Banner Component

This component is used to display the hero banner for the dgx-homepage app.

## Version
> v0.2.1

## Usage

> Require `dgx-homepage-content-banner` as a dependency in your `package.json` file.

```sh
"dgx-homepage-content-banner": "git+ssh://git@bitbucket.org/NYPL/dgx-homepage-content-banner.git#master"
```

> Once installed, import the component in your React Application.

```sh
import ContentBanner from 'dgx-homepage-content-banner';
```

## Props

> You may initialize the component with the following properties:

```sh
<ContentBanner
  className="hpContentBanner" ## String
  lang="en" ## String representation of the language (default: en)
  items={[]} ## Array of items, only the first item will be displayed (default: [])
  error={errorObject} ## Object containing `tag`, `title` and `description` fields for the error
  gaClickEvent={gaClickFunc()} ## Function for Google Analytics click events. Action is `Hero` and Label is determined by the URL
/>
```

> An example of an individual item object:

```sh
  {
    title: {
      type: 'text-group',
      en: {
        text: 'title01',
      },
    },
    category: {
      type: 'text-group',
      en: {
        text: 'category01',
      },
    },
    description: {
      type: 'text-group',
      en: {
        text: 'description01',
      },
    },
    shortTitle: {
      en: {
        text: 'short title 01',
      },
    },
    date: {
      en: {
        text: 'Tues, Apr 19 | 7 PM',
      },
    },
    location: 'Schwarzman Building',
    image: {
      bannerImage: {
        'full-uri': 'https://petrol.nypl.org/sites/default/files/desktop.carousel.ez-3.jpg',
        description: 'Alt for image 01',
        alt: 'Alt for image 01',
      },
      mobileBannerImage: {
        'full-uri': ' https://petrol.nypl.org/sites/default/files/booklist_mobile.jpg',
        description: 'Alt for image 01',
        alt: 'Alt for image 01',
      },
    },
    link: 'http://www.nypl.org/voices/audio-video/stories',
  }
```

## Local Development Setup
Run `npm install` to install all dependencies from your package.json file.

Next, startup the Webpack Development Server by running `npm start`. Visit `localhost:3000` in your web browser and begin coding.

> **NOTE:** We are currently using Webpack Hot Reload Server configuration which allows you to change your code without restarting your browser.

Once you have completed any code updates, ensure to run `npm run build` in your terminal. This will build/bundle all your code in the `dist/` path via Webpack.
