## Changelog

### v0.2.1
- Updating the `h2` to an `h1`.

### v0.2.0
- Upgrading to React 15.

### v0.1.0
#### Added
- Added support for Google Analytics click events via `gaClickEvent` function property.

#### Changed
- Updated README to include a Changelog
