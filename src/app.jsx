import React from 'react';
import { render } from 'react-dom';
// Import components
import ContentBanner from './components/ContentBanner/ContentBanner.jsx';
// Stylesheet used for development mode
import './styles/styles.scss';

// Used to mock gaClick event
const gaClickTest = () => (
  (action, label) => {
    console.log(action);
    console.log(label);
  }
);

class App extends React.Component {
  onImageClick(event) {
    console.debug('clicked on image ', event.target);
  }

  render() {
    // Dummy content
    const items = [
      {
        title: {
          type: 'text-group',
          en: {
            text: 'title01',
          },
          es: {
            text: 'title01',
          },
        },
        category: {
          type: 'text-group',
          en: {
            text: 'category01',
          },
          es: {
            text: 'category01',
          },
        },
        description: {
          type: 'text-group',
          en: {
            text: 'description01',
          },
          es: {
            text: 'description01',
          },
        },
        shortTitle: {
          en: {
            text: 'short title 01',
          },
        },
        image: {
          bannerImage: {
            'full-uri': 'https://petrol.nypl.org/sites/default/files/desktop.carousel.ez-3.jpg',
            description: 'Alt for image 01',
            alt: 'Alt for image 01',
          },
          mobileBannerImage: {
            'full-uri': ' https://petrol.nypl.org/sites/default/files/booklist_mobile.jpg',
            description: 'Alt for image 01',
            alt: 'Alt for image 01',
          },
        },
        link: 'http://www.nypl.org/voices/audio-video/stories',
      },
      {
        title: {
          type: 'text-group',
          en: {
            text: 'title02',
          },
          es: {
            text: 'title02',
          },
        },
        category: {
          type: 'text-group',
          en: {
            text: 'category02',
          },
          es: {
            text: 'category02',
          },
        },
        description: {
          type: 'text-group',
          en: {
            text: 'description02',
          },
          es: {
            text: 'description02',
          },
        },
        date: {
          en: {
            text: 'Tues, Apr 19 | 7 PM',
          },
        },
        location: 'Schwarzman Building',
        shortTitle: {
          en: {
            text: 'short title 02',
          },
        },
        image: {
          bannerImage: {
            'full-uri': 'https://petrol.nypl.org/sites/default/files/desktop.carousel.ez-6.jpg',
            description: 'Alt for image 02',
            alt: 'Alt for image 02',
          },
          mobileBannerImage: {
            'full-uri': 'https://petrol.nypl.org/sites/default/files/roseanne_cash_mobile.jpg',
            description: 'Alt for image 02',
            alt: 'Alt for image 02',
          },
        },
        link: 'http://www.nypl.org/research/information/research-matters',
      },
      {
        title: {
          type: 'text-group',
          en: {
            text: 'title03',
          },
          es: {
            text: 'title03',
          },
        },
        category: {
          type: 'text-group',
          en: {
            text: 'category03',
          },
          es: {
            text: 'category03',
          },
        },
        shortTitle: null,
        description: {
          type: 'text-group',
          en: {
            text: 'description03',
          },
          es: {
            text: 'description03',
          },
        },
        image: {
          bannerImage: {
            'full-uri': 'https://petrol.nypl.org/sites/default/files/desktop.carousel.ez-4.jpg',
            description: 'Alt for image 03',
            alt: 'Alt for image 03',
          },
          mobileBannerImage: {
            'full-uri': 'https://petrol.nypl.org/sites/default/files/magic_flute_mobile.jpg',
            description: 'Alt for image 03',
            alt: 'Alt for image 03',
          },
        },
        link: 'http://digitalcollections.nypl.org/',
      },
    ];

    return (
      <ContentBanner
        ref={i => (this.contentBanner = i)}
        items={items}
        onClick={this.onImageClick}
        onImageLoad={this.onImageLoad}
        gaClickEvent={gaClickTest()}
      />
    );
  }
}

render(<App />, document.getElementById('banner'));
